namespace Unibo.Oop.Utils
{
    public static class ImmutableMatrix
    {
        public static IImmutableMatrix<TElem> Wrap<TElem>(IBaseMatrix<TElem> matrix)
        {
            return new ImmutableMatrixDecorator<TElem>(matrix);
        }
    }
}