package it.unibo.oop.mnk;

public interface MNKMatchListener {
    void onTurnEnded(TurnEventArgs args);
    void onTurnBeginning(TurnEventArgs args);
    void onMatchEnded(MatchEventArgs args);
    void onResetPerformed(MNKMatch model);
    void onErrorOccurred(Exception error);
}
